# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=bash
pkgver=5.1.16
pkgrel=2
pkgdesc="The GNU Bourne Again shell"
arch=('x86_64')
url="https://www.gnu.org/software/bash/bash.html"
license=('GPL')
groups=('base')
depends=('glibc' 'ncurses' 'readline')
optdepends=('bash-completion: for tab completion')
backup=(etc/profile.d/bash_completion.sh
    etc/bashrc
    etc/profile.d/dircolors.sh
    etc/profile.d/extrapaths.sh
    etc/profile.d/i18n.sh
    etc/profile.d/readline.sh
    etc/shells
    {etc/skel,root}/.bash_logout
    {etc/skel,root}/.bash_profile
    {etc/skel,root}/.bashrc
    etc/profile.d/umask.sh)
source=(https://ftp.gnu.org/gnu/${pkgname}/${pkgname}-${pkgver}.tar.gz
    bash_completion.sh
    bashrc
    dircolors.sh
    extrapaths.sh
    i18n.sh
    readline.sh
    shells
    skel_bash_logout
    skel_bash_profile
    skel_bashrc
    umask.sh)
sha256sums=(5bac17218d3911834520dad13cd1f85ab944e1c09ae1aba55906be1f8192f558
    c3358ba9f1ad54255a6960236b092e32c5077f658385a9d11dd46ca73f18ad2b
    456936d471ac420a3dc10dfeee631c80579cee432802a29113c53f7e39d679bc
    1c8ce873176834ecf414926bd3b4f7ae42df4065c10d9c503bc6f38894253c29
    1f4933ecd55f17e8e09985b3500807f1672eb6427e3d78cb5d0ea1ac6069cf30
    f3e273f0d78263ee2b9afe9e53f0531feeeaa50ab7592fc19bf339bd91a812fc
    27e0c2bec3a7bee2705804815b98700e1231e373171e9151bdd211cbfbff9e72
    1db1de0b837e46cac525afad0b39b41e730114294b209a023f2e7100a3efd2f7
    0d3e7affb46fd79310d2d0b8ab63cdfbf1b6fbf58fbb6cae786db5a8d62f9bfd
    b96ceb11cf69051af9876f0a8f80a100a88208d95ff4244a9eb30f076b40c436
    5b7693b9fe722063f4b2a6aece9ed71055edb3540b69d35038d27a8b17ee640f
    6ffa96e2dca75701c2e36ae1566c3b3b7e53464960be9e4b75ba730526a200b3)

build() {
    cd ${pkgname}-${pkgver}

    _bashconfig=(
        -DDEFAULT_PATH_VALUE=\'\"/usr/bin:/usr/sbin\"\'
        -DSTANDARD_UTILS_PATH=\'\"/usr/bin\"\'
        -DSYS_BASHRC=\'\"/etc/bashrc\"\'
        -DSYS_BASH_LOGOUT=\'\"/etc/bash_logout\"\'
        -DNON_INTERACTIVE_LOGIN_SHELLS
    )

    export CFLAGS="${CFLAGS} ${_bashconfig[@]}"

    ${configure}               \
        --with-curses          \
        --enable-readline      \
        --without-bash-malloc  \
        --with-installed-readline
    make
}

package() {
    cd ${pkgname}-${pkgver}

    make DESTDIR=${pkgdir} install

    ln -sv bash ${pkgdir}/usr/bin/sh

    for i in bashrc shells; do
        install -vDm644 ${srcdir}/${i} ${pkgdir}/etc/${i}
    done

    install -vDm644 ${srcdir}/skel_bash_logout ${pkgdir}/etc/bash_logout

    install --directory --mode=0755 --owner=root --group=root ${pkgdir}/etc/profile.d

    install -vDm644 ${srcdir}/bash_completion.sh ${pkgdir}/etc/profile.d/bash_completion.sh

    install --directory --mode=0755 --owner=root --group=root ${pkgdir}/etc/bash_completion.d

    for ii in {dircolors,extrapaths,readline,umask,i18n}.sh; do
        install -vDm644 ${srcdir}/${ii} ${pkgdir}/etc/profile.d/${ii}
    done

    install -dv -m 0750 ${pkgdir}/root
    for iii in skel_{bash_logout,bash_profile,bashrc}; do
        install -vDm644 ${srcdir}/${iii} ${pkgdir}/etc/skel/.${iii#*_}
        install -vDm644 ${srcdir}/${iii} ${pkgdir}/root/.${iii#*_}
    done

    dircolors -p > ${pkgdir}/etc/dircolors
}
